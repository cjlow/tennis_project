import argparse
import os
import time
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import pandas as pd
from multiprocessing import Pool
import traceback

CHROME_DRIVER_PATH = "/home/charles/chromedriver"

def get_tourney_results(url, sleepTime=10):
    # Start up
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--incognito')
    options.add_argument('--headless')
    driver = webdriver.Chrome(CHROME_DRIVER_PATH, options=options)
    driver.implicitly_wait(sleepTime)

    try:
        driver.get(url)
        driver.implicitly_wait(5)
        tourneyTitle = driver.find_element_by_class_name("tourney-title").get_attribute("innerText")
        tourneyLocation = driver.find_element_by_class_name("tourney-location").get_attribute("innerText")
        tourneyDates = driver.find_element_by_class_name("tourney-dates").get_attribute("innerText")
        print(tourneyTitle + " @ " + tourneyLocation + " during " + tourneyDates)

        scoresTable = driver.find_element_by_class_name("scores-results-content")
        scores = scoresTable.find_elements_by_css_selector("tr")
        winner_list = []
        loser_list = []
        for scoreRow in scores:
            names = scoreRow.find_elements_by_class_name("day-table-name")
            if (len(names) == 0):
                continue
            else:
                winner = names[0].get_property("innerText")
                winner_list.append(winner)
                loser = names[1].get_property("innerText")
                loser_list.append(loser)
                print("{} DEFEAT {}".format(winner,loser))

        df = pd.DataFrame({"winner":winner_list, "loser":loser_list})
        print(df)
        file_name = tourneyTitle + "|" + tourneyLocation + "|" + tourneyDates + ".csv"
        df.to_csv("data/" + file_name, index=False)
        driver.close()
    except:
        traceback.print_exc()
        driver.close()

def scrape_data(sleepTime=10, dump=False):
    print('SCRAPING DATA')
    print('You should see an incognito Chrome window appear.')
    print('Click on and focus that window while the scraping process is running.')
    print('Please do not do anything else your computer until the scrape is complete.')
    print('It also helps to fullscreen the window as it loads more cards.')

    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--incognito')
    # options.add_argument('--headless')
    driver = webdriver.Chrome(CHROME_DRIVER_PATH, options=options)
    driver.implicitly_wait(sleepTime)

    driver.get("https://www.atptour.com/en/scores/results-archive")

    try:

        for yr in range(2018, 1949, -1):
            # Go to year
            dropDown = driver.find_element_by_class_name("dropdown-label")
            dropDown.click()
            driver.implicitly_wait(1)
            dropDownTable = driver.find_element_by_id("resultsArchiveYearDropdown")
            yr = "'" + str(yr) + "'"
            css_input = "[data-value={}]".format(yr)
            currentYearList = dropDownTable.find_elements_by_css_selector(css_input)
            for currentYear in currentYearList:
                if currentYear.get_attribute("class") == "dropdown-default-label":
                    continue
                else:
                    currentYear.click()
            goButton = driver.find_element_by_class_name("filter-submit")
            goButton.click()
            driver.implicitly_wait(5)

            # Get results for year
            results = driver.find_elements_by_class_name("tourney-result")
            print(len(results))
            urls = []
            for element in results:
                details = element.find_elements_by_class_name("tourney-details")
                url = details[-1].find_element_by_class_name("button-border").get_attribute("href")
                urls.append(url)

            # Spawn threads
            p = Pool(10)
            p.map(get_tourney_results, urls)

        # Close Driver
        driver.close()

    except Exception:
        traceback.print_exc()
        driver.close()

if __name__=="__main__":
    scrape_data(sleepTime=10)




