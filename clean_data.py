import pandas as pd
import os
import numpy as np

data_files = os.listdir("data")
df = pd.DataFrame({})
for file in data_files:
    try:
        tourney, location, date = file.rstrip(".csv").split("|")
        temp = pd.read_csv("data/" + file)
        temp['tourney'] = tourney
        temp['location'] = location
        temp['date'] = date
        df = df.append(temp, ignore_index=True)
    except:
        continue

df['location'] = df['location'].map(lambda x: x.rstrip(","))
temp = df['date'].str.split(" - ", expand=True)
df['date'] = temp[0]

def safe_int(string):
    try:
        integer = int(string)
    except:
        integer = None
    return integer

def expand_date(df):
    temp = df['date'].str.split(".", expand=True)
    df['year'] = temp[0].map(safe_int)
    df['month'] = temp[1].map(safe_int)
    df['day'] = temp[2].map(safe_int)
    return df

df = expand_date(df)
df.to_csv("cleaned/data.csv", index=False)